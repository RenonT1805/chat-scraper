import { Chat } from "../chat";

export class Cocoforia {
  public getChats(): Chat[] {
    const result: Chat[] = [];
    const chatboxElement = document.getElementsByClassName("MuiList-root")[0];
    if (!chatboxElement) {
      return [];
    }
    const chatWrapperElements = Array.from(
      chatboxElement.getElementsByClassName("MuiListItemText-root"),
    );
    if (!chatWrapperElements) {
      return [];
    }
    chatWrapperElements.forEach((chatWrapperElement) => {
      const chat = new Chat();
      const chatElement = chatWrapperElement.getElementsByClassName(
        "MuiListItemText-secondary",
      )[0];
      if (chatElement) {
        chat.text = chatElement.textContent ? chatElement.textContent : "";
      }

      const authorWrapper = chatWrapperElement.getElementsByClassName(
        "MuiListItemText-primary",
      )[0];
      if (authorWrapper) {
        const rawAuthor = authorWrapper.textContent;
        chat.author = rawAuthor ? rawAuthor.split("-")[0].trimEnd() : "";
      }

      const timeElement = authorWrapper.getElementsByClassName(
        "MuiTypography-caption",
      )[0];
      if (timeElement && timeElement.textContent) {
        const rawTime = timeElement.textContent;
        const splitedRawTime = rawTime.split(" ");
        const rawTimeText = splitedRawTime[splitedRawTime.length - 1];
        chat.time = new Date();
        if (rawTime.includes("今日")) {
          const hourMinute = rawTimeText.split(":");
          chat.time.setHours(parseInt(hourMinute[0]), parseInt(hourMinute[1]));
        } else {
          const dayHourMinute = rawTimeText.split("/");
          chat.time.setFullYear(
            parseInt(dayHourMinute[0]),
            parseInt(dayHourMinute[1]),
            parseInt(dayHourMinute[2]),
          );
        }
      }
      result.push(chat);
    });
    return result;
  }
}
