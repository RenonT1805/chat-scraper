import { Scraper, ScraperType } from "../scraper";
import { Cocoforia } from "./cocoforia";

export class ScraperFactory {
  public static getScraper(type: ScraperType): Scraper {
    switch (type) {
      case "Cocoforia":
        return new Cocoforia();
    }
  }
}
