import { Chat } from "./chat";

export type ScraperType = "Cocoforia";

export interface Scraper {
  getChats(): Chat[];
}
