import axios from "axios";
import { Config } from "./config";

export class ChatRequest {
  public text: string = "";
  public author: string = "";
  public requestID: string = "";
}

export class Client {
  public static async postChats(chat: ChatRequest[]) {
    const config = await Config.getConfig();
    return await axios.post(config.apiOrigin + "/speak", chat);
  }
}
