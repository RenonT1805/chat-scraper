import { Client } from "./client";
import { Config } from "./config";

window.onload = function () {
  const saveButton = document.getElementById("save");
  const originBox = <HTMLInputElement>document.getElementById("origin");
  const requestIDBox = <HTMLInputElement>document.getElementById("request_id");

  if (originBox && requestIDBox && saveButton) {
    Config.getConfig().then((config) => {
      originBox.value = config.apiOrigin;
      requestIDBox.value = config.requestID;
    });

    saveButton.addEventListener("click", async function () {
      const config = await Config.getConfig();
      config.apiOrigin = originBox.value;
      config.requestID = requestIDBox.value;
      Config.save(config);
    });
  } else {
    alert("element error!!");
  }
};
