import { Chat } from "../chat";
import { ChatRequest, Client } from "../client";
import { Config } from "../config";
import { ScraperFactory } from "../scprapers/factory";

let lastChat = new Chat();

setInterval(() => {
  Config.getConfig().then((config) => {
    const scraper = ScraperFactory.getScraper("Cocoforia");
    const chats = scraper.getChats();
    if (chats.length === 0) {
      return;
    }

    let idx = -1;
    chats.forEach((v, i) => {
      if (
        v.author === lastChat.author &&
        v.text === lastChat.text &&
        v.time.getHours() === lastChat.time.getHours() &&
        v.time.getMinutes() === lastChat.time.getMinutes()
      ) {
        idx = i;
      }
    });

    if (idx !== -1 && chats.length !== idx + 1) {
      const postChats = chats.slice(idx + 1).map((v) => {
        return {
          author: v.author,
          text: v.text,
          requestID: config.requestID,
        } as ChatRequest;
      });
      Client.postChats(postChats);
    }
    lastChat = chats[chats.length - 1];
  });
}, 100);
