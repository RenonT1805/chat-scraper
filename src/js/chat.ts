export class Chat {
  public text: string = "";
  public author: string = "";
  public time: Date = new Date();
}
