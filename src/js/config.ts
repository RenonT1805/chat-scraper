import { browser } from "webextension-polyfill-ts";
import { Client } from "./client";

export class Config {
  constructor() {}

  public apiOrigin: string = "";
  public requestID: string = "";

  public static async getConfig(): Promise<Config> {
    const data = await browser.storage.local.get();
    return {
      apiOrigin: data["apiOrigin"] ? data["apiOrigin"] : "http://localhost",
      requestID: data["requestID"] ? data["requestID"] : "",
    };
  }

  public static save(config: Config) {
    browser.storage.local.set(config);
  }
}
